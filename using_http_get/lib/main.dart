import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

void main() {
  runApp(new MaterialApp(
    home: new MyGetHttpData(),
  ));
}

// Create a stateful widget
class MyGetHttpData extends StatefulWidget {
  @override
  MyGetHttpDataState createState() => new MyGetHttpDataState();
}

// Create the state for our stateful widget
class MyGetHttpDataState extends State<MyGetHttpData> {
  //final String url = "https://swapi.co/api/people";
  final String url = "http://10.0.2.2:9001/actuator/health";
  List<Service> data;
  String status="";
  Map<String, dynamic> a ;

  // Function to get the JSON data
  Future<String> getJSONData() async {
    var response = await http.get(Uri.encodeFull(url),headers: {"Accept": "application/json"});


    var jsonBody = response.body;
    // Logs the response body to the console
    print('Response: body: '+jsonBody);



    // To modify the state of the app, use this method
    setState(() {

      //var mon = SiestMonitor.fromJson(body);

      // Get the JSON data
      //var dataConvertedToJSON = json.decode(response.body);
      // Extract the required part and assign it to the global variable named data
      //status = dataConvertedToJSON['status'];
       //a = Map<String, dynamic>.from(dataConvertedToJSON['details']);

      //print('status:'+mon.contarLit().toString());
      data = SiestMonitor.fromJson(jsonBody).list;

    });

    return "Successfull";
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Siest - Monitoramento do Serviço"),
      ),
      // Create a Listview and load the data when available
      body: Column(
        children: <Widget>[
          Text('Deliver features faster'),
          Text('Craft beautiful UIs'),

          Column(
            // Stretch the cards in horizontal axis
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Card(
                child: new Container(
                  child: new Text(
                    // Read the name field value and set it in the Text widget
                    "fs",
                    // set some style to text
                    style: new TextStyle(
                        fontSize: 20.0, color: Colors.lightBlueAccent),
                  ),
                  // added padding
                  padding: const EdgeInsets.all(15.0),
                ),
              )
            ],
          ),
          Expanded(
            child: new ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, int index) {
                  return new Container(
                    child: new Center(
                        child: new Column(
                          // Stretch the cards in horizontal axis
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            new Card(
                              child: new Container(
                                child: new Text(
                                  // Read the name field value and set it in the Text widget
                                  data.elementAt(index).service,
                                  // set some style to text
                                  style: new TextStyle(
                                      fontSize: 20.0, color: Colors.lightBlueAccent),
                                ),
                                // added padding
                                padding: const EdgeInsets.all(15.0),
                              ),
                            )
                          ],
                        )),
                  );
                })

          ),
        ],
      )



    );
  }

  @override
  void initState() {
    super.initState();

    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
}


class SiestMonitor {
  String status;
  //Set - an unordered collection of unique items
  var services = <Service>{};
  //List - ordered group of objects.
  List<Service> list = new List();

  SiestMonitor();

  SiestMonitor.fromJson(String json) {
    Map<String, dynamic> base1 =  jsonDecode(json);
    this.status=base1['status'];
    Map<String, dynamic> detail = base1['details'];
    carregarServicos(detail);

  }

  void carregarServicos(Map<String, dynamic> serv) {
    void iterateMapEntry(key, value) {
      Service s = new Service(key, value['status']);
      services.add(s);
      list.add(s);

    }
    serv.forEach(iterateMapEntry);
  }

  int contar(){
    return services.length;
  }

  int contarLit(){
    return list.length;
  }

}

class Service {
  String service;
  String status;

  Service(this.service, this.status);

}
